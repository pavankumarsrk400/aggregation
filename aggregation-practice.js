//mongoimport --db=countries --collection=people data.json --jsonArray
//mongoimport --db=countries --collection=people C:\Users\Jeyapriya\Desktop\Aggregation\data.json --jsonArray
//mongoimport.exe  --db=sathish --collection=new C:\Users\Jeyapriya\Desktop\Aggregation\person.json --jsonArray
$match-where
$group-group by
$project-Select
$sort-order by asc or desc

1.find all people
db.people.aggregate([{$match:{}}]).pretty(); o/p-it will fetch all objects in json

2.count all people
db.people.aggregate([
    {$count:"Total People Count"}
])
o/p-
{ "Total People Count" : 6954 }

3.group by age 
db.people.aggregate([
    {$group:{_id:"$age"}},
    {$count:"age"}
])
o/p-
{ "age" : 21 }

4.group by eye color
db.people.aggregate([
    {$match:{age:{$gte:25}}},
    {$group:{_id:{eyeColor:"$eyeColor",age:"$age"}}},
    {$count:"eyeColorAndAge"}
])
o/p-
{ "eyeColorAndAge" : 48 }

5-sort using gender,age,eyeColor
db.people.aggregate([
    { $sort: { age: 1, gender: -1, eyeColor: 1 } }
]).pretty();
o/p-
{
    "_id" : ObjectId("5f53beed3e5787227aec09fd"),
    "uan" : "c36b3a83-3922-478a-99a5-7aa103940b69",
    "isActive" : true,
    "age" : 20,
    "name" : "Ortega Warren",
    "gender" : "male",
    "villageName" : "Katchur",
    "registered" : "Sat Sep 16 1972 09:12:30 GMT+0530 (India Standard Time)",
    "eyeColor" : "black",
    "favoriteFruit" : "apple",
    "company" : {
            "title" : "HP",
            "email" : "ortegawarren@waab.com",
            "phone" : "(961) 417-3473",
            "location" : {
                    "country" : "India",
                    "address" : "Ebony Court"
            }
    }
}for above query o/p is many so i entered single o/p

6.list all the peoples without blue eye color
db.people.aggregate([
    { $match: { eyeColor: { $ne: "blue" } } },
    {
        $group: {
            _id: {
                eyeColor: "$eyeColor",
                favoriteFruit: "$favoriteFruit"
            }
        }
    },
    { $sort: { "_id.eyeColor": 1, "_id.favoriteFruite": -1 } }
]) 
o/p-
{ "_id" : { "eyeColor" : "black", "favoriteFruit" : "cherry" } }
{ "_id" : { "eyeColor" : "black", "favoriteFruit" : "bannane" } }
{ "_id" : { "eyeColor" : "black", "favoriteFruit" : "apple" } }
{ "_id" : { "eyeColor" : "brown", "favoriteFruit" : "bannane" } }
{ "_id" : { "eyeColor" : "brown", "favoriteFruit" : "apple" } }
{ "_id" : { "eyeColor" : "brown", "favoriteFruit" : "cherry" } }
{ "_id" : { "eyeColor" : "green", "favoriteFruit" : "cherry" } }
{ "_id" : { "eyeColor" : "green", "favoriteFruit" : "apple" } }
{ "_id" : { "eyeColor" : "green", "favoriteFruit" : "bannane" } }

7-get limited filds
db.people.aggregate([
    {$project:{isActive:1,name:1,gender:1, _id:0}} 
])
o/p-
{ "isActive" : true, "name" : "Earnestine Freeman", "gender" : "female" }
{ "isActive" : true, "name" : "Ward Daniels", "gender" : "male" }
{ "isActive" : true, "name" : "Shelia Pratt", "gender" : "female" }
{ "isActive" : true, "name" : "Simmons Burton", "gender" : "male" }
{ "isActive" : false, "name" : "Holland Fields", "gender" : "male" }
{ "isActive" : false, "name" : "Wiggins Farley", "gender" : "male" }
{ "isActive" : false, "name" : "Angelita Padilla", "gender" : "female" }
{ "isActive" : false, "name" : "Alta Hull", "gender" : "female" }
{ "isActive" : false, "name" : "Jill Jimenez", "gender" : "female" }
{ "isActive" : true, "name" : "Hahn Wood", "gender" : "male" }
{ "isActive" : true, "name" : "Ila Daugherty", "gender" : "female" }
{ "isActive" : false, "name" : "Matilda Potter", "gender" : "female" }
{ "isActive" : true, "name" : "Jacobson Chaney", "gender" : "male" }
{ "isActive" : false, "name" : "Mona Dixon", "gender" : "female" }
{ "isActive" : false, "name" : "Susanna Sykes", "gender" : "female" }
{ "isActive" : false, "name" : "Melva Johnston", "gender" : "female" }
{ "isActive" : false, "name" : "Logan Griffin", "gender" : "male" }
{ "isActive" : false, "name" : "Tasha Cooper", "gender" : "female" }
{ "isActive" : true, "name" : "Twila Nichols", "gender" : "female" }
{ "isActive" : true, "name" : "Alexandra Fowler", "gender" : "female" }
Type "it" for more

8.project restructure
db.people.aggregate([
    {
        $project: {
            _id: 0,
            name: 1,
            extraMetaInfo: {
                eyes: "$eyeColor",
                company: "$company.title",
                country: "$company.location.country"
            }
        }
    }
])
o/p-
{ "name" : "Earnestine Freeman", "extraMetaInfo" : { "eyes" : "brown", "company" : "Cisco", "country" : "Australia" } }
{ "name" : "Ward Daniels", "extraMetaInfo" : { "eyes" : "green", "company" : "Apple", "country" : "India" } }
{ "name" : "Shelia Pratt", "extraMetaInfo" : { "eyes" : "green", "company" : "HP", "country" : "Australia" } }
{ "name" : "Simmons Burton", "extraMetaInfo" : { "eyes" : "brown", "company" : "Cisco", "country" : "USA" } }
{ "name" : "Holland Fields", "extraMetaInfo" : { "eyes" : "green", "company" : "Apple", "country" : "USA" } }
{ "name" : "Wiggins Farley", "extraMetaInfo" : { "eyes" : "black", "company" : "IBM", "country" : "India" } }
{ "name" : "Angelita Padilla", "extraMetaInfo" : { "eyes" : "black", "company" : "Cisco", "country" : "Japan" } }
{ "name" : "Alta Hull", "extraMetaInfo" : { "eyes" : "green", "company" : "HP", "country" : "Japan" } }
{ "name" : "Jill Jimenez", "extraMetaInfo" : { "eyes" : "brown", "company" : "Apple", "country" : "USA" } }
{ "name" : "Hahn Wood", "extraMetaInfo" : { "eyes" : "green", "company" : "Apple", "country" : "Australia" } }
{ "name" : "Ila Daugherty", "extraMetaInfo" : { "eyes" : "green", "company" : "IBM", "country" : "Japan" } }
{ "name" : "Matilda Potter", "extraMetaInfo" : { "eyes" : "green", "company" : "IBM", "country" : "India" } }
{ "name" : "Jacobson Chaney", "extraMetaInfo" : { "eyes" : "black", "company" : "HP", "country" : "USA" } }
{ "name" : "Mona Dixon", "extraMetaInfo" : { "eyes" : "black", "company" : "IBM", "country" : "Australia" } }
{ "name" : "Susanna Sykes", "extraMetaInfo" : { "eyes" : "brown", "company" : "Apple", "country" : "USA" } }
{ "name" : "Melva Johnston", "extraMetaInfo" : { "eyes" : "green", "company" : "Apple", "country" : "Australia" } }
{ "name" : "Logan Griffin", "extraMetaInfo" : { "eyes" : "brown", "company" : "Cisco", "country" : "Japan" } }
{ "name" : "Tasha Cooper", "extraMetaInfo" : { "eyes" : "black", "company" : "IBM", "country" : "Australia" } }
{ "name" : "Twila Nichols", "extraMetaInfo" : { "eyes" : "black", "company" : "Apple", "country" : "USA" } }
{ "name" : "Alexandra Fowler", "extraMetaInfo" : { "eyes" : "black", "company" : "HP", "country" : "USA" } }
Type "it" for more

9.multi query match
db.people.aggregate([
    //stage 0 
    { $limit: 100 },
    //stage 1
    { $match: { eyeColor: { $ne: "blue" } } },
    //stage 2
    {
        $group: {
            _id: {
                eyeColor: "$eyeColor",
                favoriteFruit: "$favoriteFruit"
            }
        }
    },
    //stage 3
    { $sort: { "_id.eyeColor": 1, "_id.favoriteFruit": -1 } }
])
o/p-
"_id" : { "eyeColor" : "black", "favoriteFruit" : "cherry" } }
{ "_id" : { "eyeColor" : "black", "favoriteFruit" : "bannane" } }
{ "_id" : { "eyeColor" : "black", "favoriteFruit" : "apple" } }
{ "_id" : { "eyeColor" : "brown", "favoriteFruit" : "cherry" } }
{ "_id" : { "eyeColor" : "brown", "favoriteFruit" : "bannane" } }
{ "_id" : { "eyeColor" : "brown", "favoriteFruit" : "apple" } }
{ "_id" : { "eyeColor" : "green", "favoriteFruit" : "cherry" } }
{ "_id" : { "eyeColor" : "green", "favoriteFruit" : "bannane" } }
{ "_id" : { "eyeColor" : "green", "favoriteFruit" : "apple" } }

10.accumulated count by fruit
db.people.aggregate([
    {
        $group:{
            _id:"$age",
            count:{$sum:1}   
            }
        }
])

db.people.aggregate([
    {
        $group:{
            _id:"$favoriteFruit",
            count:{$sum:1}
            }
        }
])
o/p-
{ "_id" : "cherry", "count" : 2267 }
{ "_id" : "bannane", "count" : 2360 }
{ "_id" : "apple", "count" : 2327 }

11.average age
db.people.aggregate([
    {
        $group: {
            _id: "$favoriteFruit",
            avgAge: { $avg: "$age" }
        }
    }
])

db.people.aggregate([
    {
        $group: {
            _id: "$company.location.country",
            avgAge: { $avg: "$age" }
        }
    }
])
o/p-
{ "_id" : "Australia", "avgAge" : 30.288293535235876 }
{ "_id" : "USA", "avgAge" : 29.914204545454545 }
{ "_id" : "Japan", "avgAge" : 29.948823529411765 }
{ "_id" : "India", "avgAge" : 29.75464265616207 }

12.create new collection
db.people.aggregate([
    { $group: { _id: { age: "$age", eyeColor: "$eyeColor" } } },
    { $out: "peopleAggregationAgeEyecolor" }    //if this collection does not exist mongodb will create one.
])
o/p- it will create new collection as peopleAggregationAgeEyecolor


import zip file 

mongoimport --db=pavan --collection=kumar C:\Users\Jeyapriya\Desktop\Aggregation\zip-file.json

1.find all cities   
db.kumar.aggregate(
    {
      $group: {
        "_id": "$city"
      }
    }
  )
o/p- "_id" : "WAYNESBORO" }
{ "_id" : "BEARDSTOWN" }
{ "_id" : "ARBYRD" }
{ "_id" : "COUNSELOR" }
{ "_id" : "HOPE HULL" }
{ "_id" : "BURGOON" }
{ "_id" : "LETCHER" }
{ "_id" : "BRIDGEPORT" }
{ "_id" : "WAUKEE" }
{ "_id" : "RANDSBURG" }
{ "_id" : "FOX RIVER GROVE" }
{ "_id" : "OWINGS" }
{ "_id" : "DRAKESBORO" }
{ "_id" : "VALMEYER" }
{ "_id" : "ALLISON PARK" }
{ "_id" : "PINEY FLATS" }
{ "_id" : "FITHIAN" }
{ "_id" : "CASPAR" }
{ "_id" : "TYNGSBORO" }
{ "_id" : "HATTON" }
Type "it" for more

Try to get "cityName" in label of the response

db.kumar.aggregate({ "$group": { "_id": "$city"}},
{"$addFields":{"cityname": "$_id"}},
{"$project": {"_id":0}})
 o/p-
 { "cityname" : "VALLECITOS" }
{ "cityname" : "BASS LAKE" }
{ "cityname" : "WAIMANALO" }
{ "cityname" : "QUARTZ HILL" }
{ "cityname" : "NORTH SALEM" }
{ "cityname" : "JAYTON" }
{ "cityname" : "EAST SPRINGFIELD" }
{ "cityname" : "PAPAALOA" }
{ "cityname" : "HARRAH" }
{ "cityname" : "DEWEYVILLE" }
{ "cityname" : "LURAY" }
{ "cityname" : "IGIUGIG" }
{ "cityname" : "ALLENSVILLE" }
{ "cityname" : "GILMAN HOT SPRIN" }
{ "cityname" : "GLAZIER" }
{ "cityname" : "APACHE JUNCTION" }
{ "cityname" : "WISNER" }
{ "cityname" : "HOUSE" }
{ "cityname" : "BLOOM" }
{ "cityname" : "LANGFORD" }
Type "it" for more

2.find top 10 city population
db.kumar.aggregate([
    { "$group": { "_id": { "state": "$state", "city": "$city" }, "city_pop": { "$sum": "$pop" } } },
    { "$group": { "_id": "$_id.state", "avgCityPop": { "$avg": "$city_pop" } } },
    { "$sort": { "avgCityPop": -1 } },
    { "$limit": 10 }
])
o/p-
{ "_id" : "DC", "avgCityPop" : 303450 }
{ "_id" : "CA", "avgCityPop" : 27756.42723880597 }
{ "_id" : "FL", "avgCityPop" : 27400.958963282937 }
{ "_id" : "AZ", "avgCityPop" : 20591.16853932584 }
{ "_id" : "RI", "avgCityPop" : 19292.653846153848 }
{ "_id" : "NV", "avgCityPop" : 18209.590909090908 }
{ "_id" : "HI", "avgCityPop" : 15831.842857142858 }
{ "_id" : "NJ", "avgCityPop" : 15775.89387755102 }
{ "_id" : "MA", "avgCityPop" : 14855.37037037037 }
{ "_id" : "CT", "avgCityPop" : 14674.625 }

3.largest population
db.kumar.aggregate([
    { "$group": { "_id": { "state": "$state", "city": "$city" }, "city_pop": { "$sum": "$pop" } } },
    { "$sort": { "city_pop": 1 } }
])

4.find population
db.kumar.aggregate([
    { "$group": { "_id": "$state", "totalPop": { "$sum": "$pop" } } },
    { "$match": { "totalPop": { "$gte": 10 * 1e6 } } }
])
o/p-
"_id" : "IL", "totalPop" : 11427576 }
{ "_id" : "FL", "totalPop" : 12686644 }
{ "_id" : "CA", "totalPop" : 29754890 }
{ "_id" : "NY", "totalPop" : 17990402 }
{ "_id" : "PA", "totalPop" : 11881643 }
{ "_id" : "OH", "totalPop" : 10846517 }
{ "_id" : "TX", "totalPop" : 16984601 }
>